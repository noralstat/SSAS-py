import os
import winreg
import sys
import re
import subprocess
import json
import argparse
import tkinter as tk
from tkinter import filedialog
import ctypes


if not ctypes.windll.shell32.IsUserAnAdmin():
    print("You need to run this program as an administrator.")
    sys.exit(1)


# Check if config file exists
if os.path.isfile('config.json'):
    with open('config.json', 'r') as f:
        config = json.load(f)
    steam_dir = config['steam_dir']
    steam_exe = os.path.join(steam_dir, 'Steam.exe')
else:
    # Prompt user to select Steam directory
    root = tk.Tk()
    root.withdraw()
    steam_dir = filedialog.askdirectory(title='Select Steam Directory')
    steam_exe = os.path.join(steam_dir, 'Steam.exe')
    config = {'steam_dir': steam_dir}
    with open('config.json', 'w') as f:
        json.dump(config, f)

verbose = False

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

def switch_steam_account(username):
    # Edit loginusers.vdf
    users_conf = os.path.join(steam_dir, 'config', 'loginusers.vdf')
    if verbose:
        print(f"opening loginuser.vdf in {steam_dir}")
    with open(users_conf, 'r') as f:
        data = f.read()
    new_data = data.replace('"mostrecent"\t\t"1"', '"mostrecent"\t\t"0"')
    new_data = new_data.replace(f'"AccountName"\t\t"{username}"',
                                f'"AccountName"\t\t"{username}"\n\t"mostrecent"\t\t"1"')
    with open(users_conf, 'w') as f:
        f.write(new_data)
        if verbose:
            print(f"editing loginuser.vdf in {steam_dir}")

    # Edit registry settings
    key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Valve\Steam', 0, winreg.KEY_WRITE)
    winreg.SetValueEx(key, 'AutoLoginUser', 0, winreg.REG_SZ, username)
    winreg.SetValueEx(key, 'RememberPassword', 0, winreg.REG_DWORD, 1)
    winreg.CloseKey(key)
    if verbose:
        print(f"editing registry...")

    # Close running Steam processes
  #  for proc in os.popen('tasklist').readlines():
  #      if 'steam' in proc.lower():
   #         os.system(f'taskkill /F /IM {proc.split()[0]} /T')
    if verbose:
        print(f"killing steam.. ")

    cmd = 'taskkill /F /FI "IMAGENAME eq steam*" /T > nul'
    subprocess.call(cmd, shell=True)

    # Restart Steam
    if verbose:
        print(f"restarting steam.. ")
    os.startfile(steam_exe)

    input(f"Switch to {username} Succeeded! Press Enter to continue... ")


if __name__ == '__main__':

    while True:
        clear()
        try:
            user_conf = os.path.join(steam_dir, 'config', 'loginusers.vdf')
        except:
            e = input("ERROR: no config.json file present!")
            exit()
        with open(user_conf, 'r') as f:
            data = f.read()
        pattern = r'"AccountName"\s+"(.+)"'
        accounts = re.findall(pattern, data)

        print("Available Steam accounts:")
        for i, account in enumerate(accounts):
            print(f"{i + 1}. {account}")

        while True:
            selection = input("Enter the number of the account you want to switch to: ")
            try:
                index = int(selection) - 1
                if 0 <= index < len(accounts):
                    username = accounts[index]
                    break
                else:
                    print("Invalid selection. Please enter a number between 1 and", len(accounts))
            except ValueError:
                print("Invalid input. Please enter a number.")

        switch_steam_account(username)
