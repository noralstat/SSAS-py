import os
import json
import tkinter as tk
from tkinter import filedialog

# Load the config file
with open('config.json', 'r') as f:
    config = json.load(f)

# Create a root window for the GUI
root = tk.Tk()
root.withdraw()  # Hide the root window

# Open a file dialog to select the Steam directory
steam_dir = filedialog.askdirectory(title='Select your Steam directory')
if steam_dir:
    # Update the config file with the selected Steam directory
    config['steam_dir'] = steam_dir
    with open('config.json', 'w') as f:
        json.dump(config, f, indent=4)
